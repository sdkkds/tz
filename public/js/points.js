document.addEventListener('DOMContentLoaded', function() { // Аналог $(document).ready(function(){
    $('input#findPoints').click(function(e){
        $.ajax({
            url: "api/points-in-radius",
            method: "post",
            data: {
                latitude : $('#latitude').val(),
                longitude : $('#longitude').val(),
                radius : $('#radius').val()
            },
            success: function(result){
                let out = "";
                $.each(result, function( key, value ) {
                    out += '<tr><td>'+(key+1)+': latitude = '+ value.latitude + ", longitude = " + value.longitude +'</td></tr>';
                });
                $(".result").html(out);
            },
            error: function (error){
                console.log(error);
            }
        });
    })
});