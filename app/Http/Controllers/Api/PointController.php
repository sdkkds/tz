<?php

namespace App\Http\Controllers\Api;

use App\Point;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PointController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return array
     */
    public function pointsInRadius(Request $request)
    {
        $x0 = $request->post()['latitude'];
        $y0 = $request->post()['longitude'];
        $r = $request->post()['radius'];
        $points = Point::all();
        $pointsInRadius = collect();
        foreach($points as $point){
            if(pow( $point->latitude - $x0, 2) + pow($point->longitude - $y0, 2) <= pow($r,2)){
                $pointsInRadius->add($point);
            }
        }
        return $pointsInRadius->all();
    }
}
