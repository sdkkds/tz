<?php

use Illuminate\Database\Seeder;

class PointSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = 'points';
        $model = 'App\Point';
        DB::table($table)->delete();

        factory($model, 500)->create();
    }
}
