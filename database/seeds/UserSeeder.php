<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = 'users';
        $model = 'App\User';
        DB::table($table)->delete();

        factory($model)->create([
            'name' => "admin",
            'email' => "admin@admin.com",
            'password' => Hash::make("admin")
        ]);
    }
}
