@extends('layouts.app')
@section('navbar')
    @include('app.navbar')
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @include('points')
        </div>
    </div>
</div>
@endsection
