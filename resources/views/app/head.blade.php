<head>
    @include('app.meta')
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="icon" type="image/png" href="https://puppetlabs.com/favicon.ico">
    @include('app.scripts')
    @section('scripts')@show
    @include('app.fonts')
    @include('app.styles')
</head>