<body>
<div id="app">
    @section('navbar')@show
    <main class="py-4">
        @yield('content')
    </main>
</div>
</body>