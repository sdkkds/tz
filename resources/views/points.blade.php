<div class="points-title">
    <p>
        <span >Find</span>
        <span class="g-green">P</span>
        <span class="g-red">o</span>
        <span class="g-blue">i</span>
        <span class="g-yellow">n</span>
        <span class="g-blue">t</span>
        <span class="g-green">s</span>
    </p>
</div>
<div class="points-container">
    <input type="text" id="latitude" placeholder="@lang('points.latitudeInputPlaceholder')">
    <input type="text" id="longitude" placeholder="@lang('points.longitudeInputPlaceholder')">
    <input type="text" id="radius" placeholder="@lang('points.radiusInputPlaceholder')">
    <input type="button" id="findPoints" value="@lang('points.findPoints')">
    <div class="result"></div>
</div>
@section('scripts')
    <script src="{{ asset('js/points.js') }}" defer></script>
@endsection