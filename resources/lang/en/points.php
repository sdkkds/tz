<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Points Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the points
    | You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'latitudeInputPlaceholder' => 'Input latitude',
    'longitudeInputPlaceholder' => 'Input longitude',
    'radiusInputPlaceholder' => 'Input radius',
    'findPoints' => 'Find points',

];
